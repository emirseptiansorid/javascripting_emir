pets = ['cat','dog','rat']

//cara 1
//for (let i = 0; i < pets.length; i++) {
//    pets[i] = pets[i] + 's'
//}

//cara 2
//const mapResult = pets.map(function (element){return element + "s"})

//cara 3
const mapResult = pets.map(element => element + "s")

console.log(mapResult)
